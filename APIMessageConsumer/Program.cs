using APIMessageConsumer;
using MassTransit;
using MassTransit.MultiBus;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//builder.Services.
builder.Services.AddMassTransit(conf =>
{
    conf.AddConsumer<WelcomeConsumer>();
    conf.UsingRabbitMq((ctx, cfg) =>
    {
        cfg.Host("amqps://dipo:global01@b-1350ae25-7ed0-4866-805c-fa5798bd8353.mq.eu-west-1.amazonaws.com");
        cfg.ReceiveEndpoint("heyo-Bruv", c =>
        {
            c.ConfigureConsumer<WelcomeConsumer>(ctx);
        });
    });
});

builder.Services.AddMassTransitHostedService();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();