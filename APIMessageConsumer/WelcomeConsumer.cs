using System;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Domain;

namespace APIMessageConsumer
{
    public class WelcomeConsumer : IConsumer<UserInformation>
    {
        public async Task Consume(ConsumeContext<UserInformation> context)
        {
            await Console.Out.WriteLineAsync(context.Message.Name);
        }
    }
}