﻿using System;
using System.Text;
using AccountConsumer;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

var factory = new ConnectionFactory()
{
    Uri = new Uri("amqps://dipo:global01@b-1350ae25-7ed0-4866-805c-fa5798bd8353.mq.eu-west-1.amazonaws.com")
};

using var connection = factory.CreateConnection();
using var channel = connection.CreateModel();

AccountQueueDeclareConsumer.Consume(channel);
Console.ReadLine();