using System;
using System.Text;
using System.Text.Json;
using RabbitMQ.Client;
using RabbitMQ.Domain;

namespace AccountsProducer
{
    public class AccountQueueDeclaration
    {
        public static void Publish(IModel channel)
        {
            channel.QueueDeclare("test-queue-we-outchea",
                durable: true,
                false,
                false,
                null);

            for (int i = 1; i <= 10; i++)
            {
                var message = new ProducerManager()
                {
                    Producer = $"Agent {i}",
                    Text = $"Welcome to Rabbit {Guid.NewGuid()}",
                    Id = i,
                    QueueStatus = (QueueStatus)Enum.ToObject(typeof(QueueStatus) , i%3)
                };

                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message));

                channel.BasicPublish("", "test-queue-we-outchea", null, body);
                Console.WriteLine($"posted: {i}");
                // Thread.Sleep(1000);
            }
        }
    }
}