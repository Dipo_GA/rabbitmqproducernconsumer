using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Domain;

namespace MicroserviceMessageProducer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WelcomeController : ControllerBase
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public WelcomeController(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }
        
        [HttpPost]
        public async Task<IActionResult> WelcomeUsers(UserInformation userInformation)
        {
           await _publishEndpoint.Publish<UserInformation>(userInformation);
            return Ok();
        }
    }
}