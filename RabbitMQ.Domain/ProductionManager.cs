namespace RabbitMQ.Domain
{
    public class ProducerManager
    {
        public int Id { get; set; }
        public string Producer { get; set; }
        public string Text { get; set; }
        
        public QueueStatus QueueStatus { get; set; }
    }

    public enum QueueStatus
    {
        ThrowException = 0,
        Active,
        Delay
    }
}