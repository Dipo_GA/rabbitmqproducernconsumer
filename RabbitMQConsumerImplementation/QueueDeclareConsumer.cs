using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Domain;

namespace RabbitMQConsumerImplementation
{
    public class QueueDeclareConsumer
    {
        public static void Consume(IModel channel)
        {
            channel.QueueDeclare("test-queue-dipo",
                durable: true,
                false,
                false,
                null);


            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (sender, e) =>
            {
                try
                {
                    var body = e.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);

                    var productionObj = JsonSerializer.Deserialize<ProducerManager>(message);
                    Console.WriteLine(message);
                    
                    switch (productionObj.QueueStatus)
                    {
                        case QueueStatus.ThrowException:
                            Console.WriteLine($"about to shut down. tag:    {e.DeliveryTag}");
                            throw new ArgumentException("Alas, you be beans");
                        case QueueStatus.Delay:
                            Console.WriteLine($"about to delay .....tag: {e.DeliveryTag}");
                            Thread.Sleep(300000);
                            break;
                        case QueueStatus.Active:
                            Console.WriteLine($"Active, much okay! tag: {e.DeliveryTag}");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    
                    channel.BasicAck(e.DeliveryTag, false);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            };

            channel.BasicConsume("test-queue-dipo", false, consumer);
        }
    }
}