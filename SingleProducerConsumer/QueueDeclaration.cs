using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Domain;

namespace SingleProducerConsumer
{
    public class QueueDeclaration
    {
        public static void Publish(IModel channel)
        {
            channel.QueueDeclare("test-queue-dipo",
                durable: true,
                false,
                false,
                null);

            for (int i = 1; i <= 90; i++)
            {
                var message = new ProducerManager()
                {
                    Producer = $"Agent {i}",
                    Text = $"Welcome to Rabbit {Guid.NewGuid()}",
                    Id = i,
                    QueueStatus = (QueueStatus)Enum.ToObject(typeof(QueueStatus) , i%3)
                };

                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message));

                channel.BasicPublish("", "test-queue-dipo", null, body);
                Console.WriteLine($"posted: {i}");
               // Thread.Sleep(1000);
            }
        }
    }
}